import { Component, OnInit } from "@angular/core";
import Swal from "sweetalert2";
import { QuestionsService } from "../services/questions.service";

@Component({
  selector: "app-question-list",
  templateUrl: "./question-list.component.html",
  styleUrls: ["./question-list.component.scss"],
})
export class QuestionListComponent implements OnInit {
  questions = [];
  constructor(private questionsService: QuestionsService) {}

  ngOnInit(): void {
    this.getQuestions();
  }

  getQuestions() {
    this.questionsService.getQuestions().subscribe(
      (res: any) => {
        this.questions = res;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  deleteQuestion(id: string) {
    Swal.fire({
      title: "¿Estás seguro de eliminar la pregunta?",
      text: "No podrás revertir esta acción",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sí, bórralo",
    }).then((result) => {
      if (result.isConfirmed) {
        return this.questionsService.deleteQuestion(id).subscribe((res) => {
          Swal.fire("Borrado!", "La pregunta ha sido borrada.", "success");
          this.getQuestions();
        });
      }
    });
  }
}
