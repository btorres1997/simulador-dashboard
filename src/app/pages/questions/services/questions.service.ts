import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";
import { HttpClient } from "@angular/common/http";

const questionsURL = `${environment.baseURL}/questions`;
@Injectable({
  providedIn: "root",
})
export class QuestionsService {
  constructor(private http: HttpClient) {}

  createQuestion(question: any) {
    return this.http.post(`${questionsURL}/`, question);
  }

  getQuestions() {
    return this.http.get(`${questionsURL}/`);
  }

  getQuestionById(id: string) {
    return this.http.get(`${questionsURL}/${id}`);
  }

  updateQuestion(id: string, question: any) {
    return this.http.put(`${questionsURL}/${id}`, question);
  }

  deleteQuestion(id: string) {
    return this.http.delete(`${questionsURL}/${id}`);
  }
}
