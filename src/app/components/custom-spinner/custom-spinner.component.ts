import { Component, OnInit } from "@angular/core";
import { SpinnerService } from "../../services/spinner.service";

@Component({
  selector: "app-custom-spinner",
  templateUrl: "./custom-spinner.component.html",
  styleUrls: ["./custom-spinner.component.scss"],
})
export class CustomSpinnerComponent implements OnInit {
  isLoading$ = this.spinnerService.isLoading$;
  constructor(private spinnerService: SpinnerService) {}

  ngOnInit(): void {}
}
