import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import { FirebaseStorageService } from "../../../services/firebase-storage.service";
import { QuestionsService } from "../services/questions.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-create-question",
  templateUrl: "./create-question.component.html",
  styleUrls: ["./create-question.component.scss"],
})
export class CreateQuestionComponent implements OnInit {
  domains = [
    { value: "numerico", label: "Numérico" },
    { value: "logico", label: "Lógico" },
    { value: "atencion", label: "Antención" },
    { value: "verbal", label: "Verbal" },
  ];
  questionForm: FormGroup;
  imageForm = new FormData();
  fileURL = "";
  fileUploadComplete = false;
  fileName: string;
  publicURL: string;
  btnSaveLabel = "Guardar";
  file: File;
  fileFeedback: File;
  feedbackType = "video";

  constructor(
    private fb: FormBuilder,
    private firebaseStorageService: FirebaseStorageService,
    private questionsService: QuestionsService
  ) {}

  ngOnInit(): void {
    this.createQuestionForm();
  }

  createQuestionForm() {
    this.questionForm = this.fb.group({
      domain: [null, [Validators.required]],
      announcement: ["", [Validators.required]],
      optionA: ["", [Validators.required]],
      optionB: ["", [Validators.required]],
      optionC: ["", [Validators.required]],
      optionD: ["", [Validators.required]],
      answer: [null, [Validators.required]],
      feedbackURL: [""],
      feedbackImage: [null],
      file: [null],
    });
  }

  async saveQuestion(buttonSave: any) {
    // console.log(this.questionForm.value);
    // console.log(this.fileFeedback);
    if (this.questionForm.valid) {
      buttonSave.disabled = true;
      const question = {
        domain: this.questionForm.get("domain").value,
        announcement: this.questionForm.get("announcement").value,
        options: [
          { literal: "a", answer: this.questionForm.get("optionA").value },
          { literal: "b", answer: this.questionForm.get("optionB").value },
          { literal: "c", answer: this.questionForm.get("optionC").value },
          { literal: "d", answer: this.questionForm.get("optionD").value },
        ],
        answer: this.questionForm.get("answer").value,
        feedbackUrl: this.questionForm.get("feedbackURL").value,
        imageURL: "",
      };
      this.questionsService.createQuestion(question).subscribe(
        async (res) => {
          let url = "";
          let feedbackUrl = "";

          if (
            this.questionForm.get("file").value === null &&
            this.questionForm.get("feedbackImage").value === null
          ) {
            Swal.fire({
              // position: "top-end",
              icon: "success",
              title: "Se ha guardado la pregunta",
              showConfirmButton: false,
              timer: 1500,
            });
          } else {
            if (this.questionForm.get("file").value) {
              url = await this.saveImage();
            }

            if (this.questionForm.get("feedbackImage").value) {
              feedbackUrl = await this.saveFeedbackImage();
            } else {
              feedbackUrl = res["feedbackUrl"];
            }

            this.questionsService
              .updateQuestion(res["_id"], {
                ...res,
                imageURL: url,
                feedbackUrl,
              })
              .subscribe((res2) => {
                Swal.fire({
                  // position: "top-end",
                  icon: "success",
                  title: "Se ha guardado la pregunta",
                  showConfirmButton: false,
                  timer: 1500,
                });
              });
          }

          this.questionForm.reset();
          buttonSave.disabled = false;
          this.file = null;
          this.fileFeedback = null;
        },
        (err) => {
          Swal.fire({
            // position: "top-end",
            icon: "error",
            title: "No se ha podido crear la pregunta",
            // showConfirmButton: false,
            // timer: 1500,
          });
          buttonSave.disabled = false;
        }
      );
    } else {
      Swal.fire({
        icon: "error",
        title: "Por favor, llene todos los campos requeridos",
      });
    }
  }

  changeFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      this.file = <File>event.target.files[0];
      // //Imagen previa
      // const reader = new FileReader();
      // reader.onload = e => this.fotoSeleccionada = reader.result;
      // reader.readAsDataURL(this.file);

      // document.getElementById('btn_Eliminar')?.removeAttribute('disabled');
      // document.getElementById('btn_Elegir')?.setAttribute('disabled', 'disabled');
    }
  }

  async saveImage(): Promise<string> {
    let nameFile = Date.now().toString() + "_" + this.file.name;
    let reader = new FileReader();
    reader.readAsDataURL(this.file);

    return new Promise((resolve) => {
      reader.onloadend = async () => {
        const url = await this.firebaseStorageService.uploadFile(
          nameFile,
          reader.result
        );
        this.fileURL = url;
        resolve(url);
      };
    });
  }

  async saveFeedbackImage(): Promise<string> {
    let nameFile = Date.now().toString() + "_" + this.fileFeedback.name;
    let reader = new FileReader();
    reader.readAsDataURL(this.fileFeedback);

    return new Promise((resolve) => {
      reader.onloadend = async () => {
        const url = await this.firebaseStorageService.uploadFile(
          nameFile,
          reader.result
        );
        this.fileURL = url;
        resolve(url);
      };
    });
  }

  changeRadioFeedback(type: string) {
    this.feedbackType = type;
    this.questionForm.get("feedbackImage").setValue(null);
    this.questionForm.get("feedbackURL").setValue("");
    this.fileFeedback = null;
    console.log(this.questionForm.value);
  }

  changeFileFeedback(event: any) {
    if (event.target.files && event.target.files[0]) {
      this.fileFeedback = <File>event.target.files[0];
    }
  }
}
