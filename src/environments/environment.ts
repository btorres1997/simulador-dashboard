// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDGVO5eBLuH0c6zBtReCaJdCjDRYuUTpk0",
    authDomain: "nic-express.firebaseapp.com",
    projectId: "nic-express",
    storageBucket: "nic-express.appspot.com",
    messagingSenderId: "1047113449249",
    appId: "1:1047113449249:web:df6b6567a72e3981758860",
    measurementId: "G-0Z8ZKJWVBZ",
  },
  baseURL: "https://www.backnicsimulador.xyz",
  // baseURL: "http://localhost:3000",
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
