import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { FullComponent } from "./layouts/full/full.component";
import { BlankComponent } from "./layouts/blank/blank.component";

export const Approutes: Routes = [
  {
    path: "",
    component: FullComponent,
    children: [
      { path: "", redirectTo: "/questions/new", pathMatch: "full" },
      {
        path: "questions",
        loadChildren: () =>
          import("./pages/questions/questions.module").then(
            (m) => m.QuestionsModule
          ),
      },
      // {
      //   path: 'component',
      //   loadChildren: () => import('./component/component.module').then(m => m.ComponentsModule)
      // }
    ],
  },
  {
    path: "**",
    redirectTo: "/starter",
  },
];
