import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

import { questionsRoutes } from "./questions.routing";
import { QuestionListComponent } from "./question-list/question-list.component";
import { CreateQuestionComponent } from "./create-question/create-question.component";
import { HttpClientModule } from "@angular/common/http";
import { FeatherModule } from "angular-feather";
import { EditQuestionComponent } from './edit-question/edit-question.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(questionsRoutes),
    HttpClientModule,
    FeatherModule,
  ],
  declarations: [QuestionListComponent, CreateQuestionComponent, EditQuestionComponent],
})
export class QuestionsModule {}
