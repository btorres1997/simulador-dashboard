import { Routes } from "@angular/router";
import { CreateQuestionComponent } from "./create-question/create-question.component";
import { QuestionListComponent } from "./question-list/question-list.component";

export const questionsRoutes: Routes = [
  {
    path: "",
    data: {
      title: "Preguntas",
      urls: [{ title: "Inicio", url: "/" }, { title: "Lista de preguntas" }],
    },
    component: QuestionListComponent,
  },
  {
    path: "new",
    data: {
      title: "Pregunta",
      urls: [{ title: "Inicio", url: "/" }, { title: "Pregunta" }],
    },
    component: CreateQuestionComponent,
  },
];
