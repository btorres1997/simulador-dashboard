import { Injectable } from "@angular/core";
import { AngularFireStorage } from "@angular/fire/compat/storage";
import firebase from "firebase/compat/app";
import "firebase/compat/storage";
@Injectable({
  providedIn: "root",
})
export class FirebaseStorageService {
  sotrageRef = firebase.app().storage().ref();
  constructor(private storage: AngularFireStorage) {}

  async uploadFile(name: string, imgBase64: any) {
    try {
      let response = await this.sotrageRef
        .child("images/" + name)
        .putString(imgBase64, "data_url");
      console.log("Respuesta: ", response);
      return await response.ref.getDownloadURL();
    } catch (error) {
      console.log(error);
      return null;
    }
  }
  //Tarea para subir archivo
  // public uploadImage(fileName: string, data: any) {
  //   return this.storage.upload(fileName, data);
  // }

  // //Referencia del archivo
  // public referenceCloudStorage(fileName: string) {
  //   return this.storage.ref(fileName);
  // }
}
